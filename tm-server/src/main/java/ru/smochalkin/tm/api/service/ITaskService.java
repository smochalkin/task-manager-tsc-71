package ru.smochalkin.tm.api.service;

import ru.smochalkin.tm.api.IBusinessService;
import ru.smochalkin.tm.dto.TaskDto;

public interface ITaskService extends IBusinessService<TaskDto> {
}
