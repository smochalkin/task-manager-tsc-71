package ru.smochalkin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.smochalkin.tm.dto.SessionDto;
import ru.smochalkin.tm.dto.UserDto;

public interface UserRepository extends JpaRepository<UserDto, String> {

    @Nullable
    UserDto findFirstByLogin(@NotNull String login);

    void deleteByLogin(@NotNull String login);

}
