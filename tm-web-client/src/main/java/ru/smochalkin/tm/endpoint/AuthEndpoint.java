package ru.smochalkin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.smochalkin.tm.api.endpoint.IAuthEndpoint;
import ru.smochalkin.tm.model.Result;
import ru.smochalkin.tm.service.UserService;

import javax.annotation.Resource;
import javax.jws.WebMethod;

@RestController
@RequestMapping("/auth")
public class AuthEndpoint implements IAuthEndpoint {

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @Override
    @WebMethod
    @GetMapping(value = "/login", produces = "application/json")
    public Result login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (Exception e) {
            return new Result(e);
        }
    }

    @Override
    @WebMethod
    @GetMapping("/logout")
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}