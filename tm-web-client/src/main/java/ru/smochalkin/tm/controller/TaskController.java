package ru.smochalkin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.api.service.ITaskService;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.CustomUser;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.model.Task;

import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @GetMapping("/task/create")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public String create(
            @AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user
    ) {
        taskService.create(user.getUserId());
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public String delete(
            @AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user,
            @PathVariable("id") String id
    ) {
        taskService.removeById(user.getUserId(),id);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public ModelAndView edit(
            @AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user,
            @PathVariable("id") String id
    ) {
        return new ModelAndView(
                "task-edit",
                "task", taskService.findById(user.getUserId(),id)
        );
    }

    @PostMapping("/task/edit")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public String edit(
            @AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user,
            @ModelAttribute("task") Task task
    ) {
        taskService.save(user.getUserId(),task);
        return "redirect:/tasks";
    }

    @ModelAttribute("viewName")
    public String getViewName() {
        return "Task edit";
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @ModelAttribute("projects")
    public List<Project> getProjects() {
        return projectService.findAll();
    }

}