package ru.smochalkin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractTaskListener;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public final class TaskCreateListener extends AbstractTaskListener {

    @Override
    @NotNull
    public String name() {
        return "task-create";
    }

    @Override
    @NotNull
    public String description() {
        return "Create new task.";
    }

    @Override
    @EventListener(condition = "@taskCreateListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter name: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("Enter description: ");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final Result result = taskEndpoint.createTask(sessionService.getSession(), name, description);
        printResult(result);
    }

}
